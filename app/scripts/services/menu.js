'use strict';
angular.module('minovateApp')
    .service('MenuService', function ($http, $q) {

        this.getMenu = getMenu;

        function getMenu() {

            var deferred = $q.defer();

            $http.get('scripts/jsons/MenuStructureRequest.json', {params : {UserId : localStorage.getItem("userid")}}).then(function (data) { deferred.resolve(data.data.d.pages); });

            return deferred.promise;

        }


    });
