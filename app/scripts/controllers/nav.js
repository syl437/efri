'use strict';

/**
 * @ngdoc function
 * @name minovateApp.controller:NavCtrl
 * @description
 * # NavCtrl
 * Controller of the minovateApp
 */
app
    .controller('NavCtrl', function ($scope, $http, $rootScope, $state, MenuService) {

        // Data

        $scope.pages = [];
        $scope.oneAtATime = false;

        // Methods

        $scope.init = init;

        // Functions

        function init () {MenuService.getMenu().then(function (data) {

            for (var i = 0; i < data.length; i++){

                data[i].customStyle = {};
                data[i].customStyle['background-color'] = data[i].BackColor;
                data[i].customStyle['font-size'] = data[i].FontSize;
                data[i].customStyle['color'] = data[i].FontColor;
                data[i].customStyle['font-family'] = data[i].FontFamily;

                for (var j = 0; j < data[i].subPages.length; j++){

                    data[i].subPages[j].customStyle = {};
                    data[i].subPages[j].customStyle['background-color'] = data[i].subPages[j].BackColor;
                    data[i].subPages[j].customStyle['font-size'] = data[i].subPages[j].FontSize;
                    data[i].subPages[j].customStyle['color'] = data[i].subPages[j].FontColor;
                    data[i].subPages[j].customStyle['font-family'] = data[i].subPages[j].FontFamily;

                }

            }

            $scope.pages = data;

            });
        }

    });
