'use strict';

/**
 * @ngdoc function
 * @name minovateApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the minovateApp
 */
app
  .controller('MainCtrl', function ($scope, $http, $translate, $state) {

      $scope.logOut = function () {
          localStorage.removeItem("userid");
          localStorage.removeItem("secondsrefresh");
          localStorage.removeItem("logo");
          $state.go('core.login');
      };
  })
