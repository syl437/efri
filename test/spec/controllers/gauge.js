'use strict';

describe('Controller: GaugeCtrl', function () {

  // load the controller's module
  beforeEach(module('minovateApp'));

  var GaugeCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    GaugeCtrl = $controller('GaugeCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(GaugeCtrl.awesomeThings.length).toBe(3);
  });
});
